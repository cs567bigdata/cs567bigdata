import unittest
import os

class FileUtils:
    def convert(self, inStr,findStr=os.path.sep,replaceStr='_'):
        return inStr.replace(findStr, replaceStr)

class TestFileUtils(unittest.TestCase):
    def test_convert(self):
        actualStr = "this" + os.pathsep + "is"+ os.pathsep + "my" + os.pathsep +"file"
        expectedStr = "this_is_my_file"
        self.assertEqual(FileUtils().convert(actualStr),expectedStr)

if __name__ == "__main__":
    unittest.main()

