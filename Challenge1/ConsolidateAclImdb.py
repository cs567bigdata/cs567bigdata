from FileUtils import FileUtils
import shutil
import os


if __name__ == "__main__":
    inDir = '/users/telabru/cs567bigdata/Challenge1/test_flat/'
    outDir = '/users/telabru/cs567bigdata/Challenge1/ConsolidatedAclImdb/'
    if not os.path.exists(os.path.dirname(outDir)):
        os.mkdir(os.path.dirname(outDir))
    count =0 
    filename2Fid = {}
    maxLines = 0
    for root, dirnames, filenames in os.walk(inDir):
        for filename in filenames:
            # get new file name path
            filenamesplit = filename.split('_')
            newFIlename = '_'.join(filenamesplit[:2])
            outFile = os.path.join(outDir,newFIlename+'.txt')
            # create fid to file link
            if outFile not in filename2Fid:
                filename2Fid[outFile] = open(outFile,'w')
            # look up and write old file txt to new file
            curFile = os.path.join(root, filename)
            print('Writing ' + curFile + ' to ' + outFile)
            filenamesplit[3] = filenamesplit[3].split('.')[0]
            txtName = '_'.join(filenamesplit)
            print(txtName)
            filename2Fid[outFile].write(txtName)
            lineInFile = 0
            with open(curFile) as f:
                for line in f:
                    filename2Fid[outFile].write(' ' + line)
                lineInFile+=1
            filename2Fid[outFile].write('\n')
            if maxLines < lineInFile:
                maxLines = lineInFile
    print('maxLine ' + str(lineInFile))

