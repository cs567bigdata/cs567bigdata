#!/usr/bin/env python

import sys
import os
from PreProcessing import PreProcessingUtils

filename = os.path.splitext(os.path.basename(os.environ['map_input_file']))[0]

# input comes from STDIN (standard input)
ppu = PreProcessingUtils()
for line in sys.stdin:
#    # remove leading and trailing whitespace
    line = line.strip()
    strSplit = line.split(' ')
    line = ' '.join(strSplit[1:]).decode("utf8")
    line = ppu.rmStopWords(line)
    line = ppu.stem(line)
    print '%s\t%s' % (filename + '_' +strSplit[0], line.encode("utf8"))
