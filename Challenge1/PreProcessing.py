import unittest
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from datetime import datetime
import os
import fnmatch

class PreProcessingUtils:
    cachedStopWords = stopwords.words("english")
    stemmer = PorterStemmer()
    def stem(self, in_str):
        return ' '.join([self.stemmer.stem(word) for word in in_str.split()])

    def rmStopWords(self, in_str):
        return ' '.join([word for word in in_str.split() if word not in self.cachedStopWords])

class PreProcessing:
    ppUtils = PreProcessingUtils()
    def fileRun(self, in_file, out_file):
        inFid = open(in_file,"r")
        if not os.path.exists(os.path.dirname(out_file)):
            os.mkdir(os.path.dirname(out_file))
        outFid = open(out_file, "w")
        for line in inFid:
            line = self.ppUtils.rmStopWords(line)
            line = self.ppUtils.stem(line)
            outFid.write(line)
        inFid.close()
        outFid.close()

    def dirRun(self,inDir,outDir):
        print(outDir)
        errorFile = open("errorfile.txt","w")
        for root, dirnames, filenames in os.walk(inDir):
            for filename in filenames:
                if filename.endswith(".txt"):
                    curFile = os.path.join(root, filename)
                    print("processing file " + outDir + curFile[len(inDir)-1:])
                    try:
                        self.fileRun(curFile,outDir + curFile[len(inDir)-1:])
                    except:
                        errorFile.write(str(curFile)+"\n")
                        continue

class TestPreProcessingUtils(unittest.TestCase):
    def test_stem(self):
        str = 'running is fun'
        expected = 'run is fun'
        self.assertEqual(PreProcessingUtils().stem(str), expected)

    def test_rmStopWords(self):
        str = 'Fred and George went to town and taught math to people'
        expectedStr = 'Fred George went town taught math people'
        self.assertEqual(PreProcessingUtils().rmStopWords(str), expectedStr)

#    def test_PreProcessFile(self):
#        # setup
#        inFileStr = "testfile."+datetime.now().strftime("%Y%M%d_%H%M%S%S")
#        outFileStr = inFileStr+"_out"
#        file = open(inFileStr,"w")
#        inputText = "Copying. did it copy the file"
#        textVerification = "copying. copy fil"
#        file.write(inputText)
#        file.close()
#        # perform task
#        PreProcessing().fileRun(inFileStr,outFileStr)
#        # read out file
#        file = open(outFileStr,"r")
#        contents = file.readline()
#        file.close()
#        self.assertEqual(contents,textVerification)
#        os.remove(inFileStr)
#        os.remove(outFileStr)

#    def test_PreProcessDir(self):
#        inDir = '/users/telabru/cs567bigdata/Challenge1/aclImdb/'
#        outDir = '/users/telabru/cs567bigdata/Challenge1/test/'
#        PreProcessing().dirRun(inDir,outDir)

if __name__ == "__main__":
    unittest.main()

