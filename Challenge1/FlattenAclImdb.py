from FileUtils import FileUtils
import shutil
import os


if __name__ == "__main__":
    inDir = '/users/telabru/cs567bigdata/Challenge1/aclImdb/'
    outDir = '/users/telabru/cs567bigdata/Challenge1/test_flat/'
    if not os.path.exists(os.path.dirname(outDir)):
        os.mkdir(os.path.dirname(outDir))
    fileUtil = FileUtils()
    for root, dirnames, filenames in os.walk(inDir):
        for filename in filenames:
            if filename.endswith(".txt"):
                curFile = os.path.join(root, filename)
                inFile = os.path.join(inDir,curFile)
                outFilename = os.path.join(outDir,fileUtil.convert(curFile[len(inDir):]))
                print("moving file " + inFile + " to " + outFilename)
                shutil.copyfile(inFile,outFilename)
