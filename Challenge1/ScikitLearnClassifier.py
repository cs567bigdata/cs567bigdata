from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import LinearSVC
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV
from sklearn.datasets import load_files
from sklearn.cross_validation import train_test_split
from sklearn import metrics
import numpy as np


if __name__ == "__main__":
    # load training data and test data
    print("load training data")
    trainData = load_files('train', shuffle=False)
    print("load test data")
    testData = load_files('test', shuffle=False)

    print("num training samples: %d" % len(trainData.data))
    print("num testing samples: %d" % len(trainData.data))

#   build pipeline to perform tfidf operation and run the SVM classifier
    print("build pipeline")
    pipeline = Pipeline(
        [('vect', TfidfVectorizer(min_df=3, max_df=0.95)),
        ('clf', LinearSVC(C=1000)),
    ])

    # Build a grid search to find out whether unigrams or bigrams are more
    # useful.
    # Fit the pipeline on the training
    print("grid search for unigrams or bigrams")
    parameters = {'vect__ngram_range': [(1, 1), (1, 2)],}
    grid_search = GridSearchCV(pipeline, parameters, n_jobs=-1)
    grid_search.fit(trainData.data, trainData.target)

    # write out mean and other related item
    print(grid_search.grid_scores_)

    # perform test on 
    print("Compute test data")
    y_predicted = grid_search.predict(testData.data)

    # Print the classification report
    print("Show classification report")
    print(metrics.classification_report(testData.target, y_predicted,
                                        target_names=trainData.target_names))

    # Print and plot the confusion matrix
    print("Show confusion matrix")
    cm = metrics.confusion_matrix(testData.target, y_predicted)
    print(cm)

    import matplotlib.pyplot as plt
    plt.matshow(cm)
    title = 'Confusion Matrix'
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(testData.target_names))
    plt.xticks(tick_marks,testData.target_names)
    plt.yticks(tick_marks,testData.target_names)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()
