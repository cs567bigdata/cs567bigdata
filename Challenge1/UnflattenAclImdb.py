import os

if __name__ == "__main__":
    inFile = '/users/telabru/cs567bigdata/Challenge1/aclimdb_file.txt'
    outDir = '/users/telabru/cs567bigdata/Challenge1/procAclImdb/'
    if not os.path.exists(os.path.dirname(outDir)):
        os.mkdir(os.path.dirname(outDir))
    with open(inFile) as file:
        for line in file:
            fileName, comment = line.split('\t', 1)
            # spilt filename
            nameSplits = fileName.split('_')
            # recreate directory structure
            topDir= os.path.join(outDir,nameSplits[0])
            typeDir= os.path.join(topDir,nameSplits[1]+'/')
            if not os.path.exists(os.path.dirname(typeDir)):
                os.makedirs(os.path.dirname(typeDir))
            # write file
            filename = os.path.join(typeDir,nameSplits[2]+'_'+nameSplits[3]+ '.txt')
            print('writing ' + filename)
            fid = open(filename,'w')
            fid.write(comment)
            fid.close()

