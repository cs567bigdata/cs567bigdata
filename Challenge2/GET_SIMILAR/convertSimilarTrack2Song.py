import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname

#from pyspark import SparkConf, SparkContext
import sqlite3

outSimilarityFile = 'convSimilarityFile.txt'
similarityFile = 'similarTracks.txt'
song2Tk = 'song_metadata_song_id_track_id-complete.txt'
songId = 'song.txt'

# build song to id map
songMap = {}
with open(songId) as f:
    for line in f:
        line = line.split()
        songMap[line[0].strip()] = line[1].strip()

# build track to song map
trak2songMap = {}
with open(song2Tk) as f:
    for line in f:
        line = line.split(',')
        trak2songMap[line[1].strip()]=line[0].strip()

outFid = open(outSimilarityFile, 'w')
lineCnt = 0
fidBadTrk = open('badTrkFile.txt','w')
with open(similarityFile) as f:
    for line in f:
        lineCnt+=1
        line = line.strip().split(',')
        for i in range(0,len(line)):
            try:
                item = songMap[trak2songMap[line[i].strip()]]
            except:
                fidBadTrk.write(line[i] + '\n')
                continue
            if i==len(line)-1:
                outFid.write(str(item) + '\n')
            else:
                outFid.write(str(item) + ',')
        if lineCnt%10000==0:
            print('count = '+str(lineCnt))
