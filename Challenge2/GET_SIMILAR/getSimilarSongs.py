import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname

from pyspark import SparkConf, SparkContext
import sqlite3

songToTrack = 'song_metadata_song_id_track_id-complete.txt'
songId = 'song.txt'
dbSimilar = '../lastfm/lastfm_similars.db'
#similarity = 0.1
similarity = 0.8
#songId = 'song.txt'

conf = SparkConf() \
  .setAppName("MovieLensALS") \
  .set("spark.executor.memory", "4g")
sc = SparkContext(conf=conf)

song2tkrdd = sc.textFile(songToTrack).map(lambda x: x.split(','))
songIdrdd = sc.textFile(songId).map(lambda x: x.split())
#print 'values=========='
#print(songIdrdd.take(2))
#print(song2tkrdd.take(2))

songTkId = song2tkrdd.join(songIdrdd)
dic = songTkId.map(lambda x: x[1]).map(lambda x: x[0]).distinct().collect()

conn = sqlite3.connect(dbSimilar)

fid = open('similarTracks.txt','w')
fullList = []
count = 0
actCnt = 0
#dic = []
#dic.append('TRCCHEP128F932DE5F')
for tid in dic:
    actCnt+=1
    if actCnt%1000==0:
        print('count - ' + str(actCnt))
    sql = "SELECT target FROM similars_src WHERE tid='%s'" % tid
    res = conn.execute(sql)
    data = res.fetchone()
    if data == None:
        continue
    data = data[0].split(',')
    writeStr = str(tid)
    similarSongList = []
    val = False
    for i in range(0,len(data),2):
        f = float(data[i+1])
        if f>= similarity:
            writeStr += ',' + str(data[i])
#            val = True
#    if val:
    fid.write(writeStr + '\n')
#        val = False
fid.close()

#print(fullList)
#print('count = ' +str(count))

