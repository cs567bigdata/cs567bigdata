package edu.unm.cs;



import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.*;
import org.apache.mahout.cf.taste.impl.recommender.*;
import org.apache.mahout.cf.taste.impl.similarity.*;
import org.apache.mahout.cf.taste.model.*;
import org.apache.mahout.cf.taste.recommender.*;
import org.apache.mahout.cf.taste.similarity.*;


import java.io.*;
import java.util.*;




public class ItemBasedRecommender {
    
    private ItemBasedRecommender() {
    }
    
    private static void performItemRecommendation(DataModel model,
                                                  ItemSimilarity itemSimilarity, String Type, Map<Integer, List<Preference>> preferenceMap) throws TasteException {
        
        
        Recommender itemRecommender = new GenericItemBasedRecommender(model,
                                                                      itemSimilarity);
        
        
        List<Double> differenceList = new ArrayList<Double>();
        try {
            // FileReader reads text files in the default encoding.
            Set<Integer> keys = preferenceMap.keySet();
            for(Integer key: keys){
                List<Preference> preferences = preferenceMap.get(key);
                for(Preference preference : preferences){
                    double estimated = itemRecommender.estimatePreference(key, preference.getItem());
                    double actual = preference.getValue();
                    if(!Double.isNaN(estimated)){
                        Double difference = estimated - actual;
                        if(!Double.isNaN(difference)){
                            differenceList.add(difference);
                        }
                    }
                }
            }
            
            
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        
        
        System.out.println("RMS " + rms(convertDoubles(differenceList)) + " for type " + Type);
        
        
    }
    
    public static double[] convertDoubles(List<Double> doubles)
    {
        double[] ret = new double[doubles.size()];
        //System.out.println(doubles);
        Iterator<Double> iterator = doubles.iterator();
        int i=0;
        while(iterator.hasNext())
        {
            ret[i] = iterator.next().doubleValue();
            i++;
        }
        
        return ret;
    }
    
    public static double rms(double[] nums){
        double ms = 0;
        for (int i = 0; i < nums.length; i++)
            ms += nums[i] * nums[i];
        ms /= nums.length;
        return Math.sqrt(ms);
    }
    
    
    
    public static void main(String[] args) throws Exception {
        String trainingFileName = args[0];
        String testFile = args[1];
        String similarityFileName = args[2];
       
        String line = null;
        
        Map<Integer,List<Preference>> preferenceMap = new HashMap<Integer, List<Preference>>();
        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader =
            new FileReader(testFile);
            
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader =
            new BufferedReader(fileReader);
            
            while((line = bufferedReader.readLine()) != null) {
                String[] tokens = line.split(",");
                String actual = tokens[2];
                
                String user = tokens[0];
                String item = tokens[1];
                try{
                    
                    Integer userValue= Integer.parseInt(user);
                    Integer itemValue = Integer.parseInt(item);
                    Double  preferenceValue = Double.parseDouble(actual);
                    if(preferenceMap.containsKey(userValue)){
                        List<Preference> list = preferenceMap.get(userValue);
                        list.add(new Preference(itemValue,preferenceValue));
                        
                    }else{
                        List<Preference> list = new ArrayList<Preference>();
                        list.add(new Preference(itemValue,preferenceValue));
                        preferenceMap.put(userValue, list);
                    }
                    
                    
                    
                    
                }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            }
            
            
            // Always close files.
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                               "Unable to open file '" +
                               testFile + "'");
        }
        catch(IOException ex) {
            System.out.println(
                               "Error reading file '"
                               + testFile + "'");
            // Or we could just do this:
            // ex.printStackTrace();
        }
        

        
        //RandomUtils.useTestSeed();
        System.out.println(new Date());
        File trainingFile = new File(trainingFileName);
        
        DataModel model = new FileDataModel(trainingFile);
        
        File similarityFile = new File(similarityFileName);
        DataModel similarityModel = new FileDataModel(similarityFile);
        
        //ItemSimilarity pearsonSimilarity = new PearsonCorrelationSimilarity(similarityModel);
        System.out.println("Starting to create EuclideanDistanceSimilarity " + new Date());
        ItemSimilarity euclideanSimilarity = new EuclideanDistanceSimilarity(model);
        System.out.println("Finished EuclideanDistanceSimilarity " + new Date());
        System.out.println("Starting to create TanimotoCoefficientSimilarity " + new Date());
        ItemSimilarity tanimotoSimilarity = new TanimotoCoefficientSimilarity(model);
        System.out.println("Finished EuclideanDistanceSimilarity " + new Date());
        System.out.println("Starting to create LogLikelihoodSimilarity " + new Date());
        ItemSimilarity logLikilihoodSimilarity = new LogLikelihoodSimilarity(model);
        System.out.println("Finished LogLikelihoodSimilarity " + new Date());
        
        //performItemRecommendation(model, pearsonSimilarity, "pearson ");
        /*
         RMS 7.093151315237881 for type euclidean
         RMS 7.136155091666502 for type tanimoto
         RMS 7.012073004831527 for type log-likelihood
         */
        
        /* using the same moded for the recommender and similairity algorithms
         RMS 7.083263969745111 for type euclidean
         RMS 7.136155091666502 for type tanimoto
         RMS 7.012073004831527 for type log-likelihood
         
         */
        System.out.println("Starting to create EuclideanDistanceSimilarity Recommender" + new Date());
        performItemRecommendation(model, euclideanSimilarity, "euclidean ", preferenceMap);
        System.out.println("Finished EuclideanDistanceSimilarity Recommender" + new Date());
        System.out.println("Starting to create TanimotoCoefficientSimilarity Recommender" + new Date());
        performItemRecommendation(model, tanimotoSimilarity, "tanimoto ",preferenceMap);
        System.out.println("Finished TanimotoCoefficientSimilarity Recommender" + new Date());
        System.out.println("Starting to create LogLikelihoodSimilarity Recommender" + new Date());
        performItemRecommendation(model, logLikilihoodSimilarity,"log-likelihood ",preferenceMap);
        System.out.println("Finished LogLikelihoodSimilarity Recommender" + new Date());
        
    }
    
    
    
    
    
}
