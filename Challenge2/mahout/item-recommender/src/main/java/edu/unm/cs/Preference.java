package edu.unm.cs;

public class Preference{
	private Integer item;
	private Double value;
	
	
	public Double getValue() {
		return value;
	}


	public void setValue(Double value) {
		this.value = value;
	}


	public Preference(Integer i, Double v){
		item = i;
		value =v;
	}
	
	
	public Integer getItem() {
		return item;
	}
	public void setItem(Integer item) {
		this.item = item;
	}
	
}