modFile = open('item_similarity_loudness.txt', 'w')

songDict = {}
similarityDict = {}

with open('song.txt') as song:
    for line in song:
        splitLine = line.split()
        songDict[splitLine[1]] = splitLine[0]

with open('song_metadata_loudness.txt') as song:
    for line in song:
        splitLine = line.split(',')
        similarityDict[splitLine[0]] = splitLine[1]


with open('train_triplets_mahout_format.txt') as f:
    for line in f:
        splitLine = line.split(',')
        songId = songDict[splitLine[1]]
        songSimilarity = '0,0'
        if songId in similarityDict:
            songSimilarity = similarityDict[songId]
            seq = (str(splitLine[0]), str(splitLine[1]), str(songSimilarity))
            modFile.write(','.join(seq))
        
        else:
            seq = (str(splitLine[0]), str(splitLine[1]), str(songSimilarity))
            modFile.write(','.join(seq)+"\n")

f.close()
modFile.close()

