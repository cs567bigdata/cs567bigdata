

#songFile = open('song.txt', 'w')
modFile = open('mod_train_triplets.txt', 'w')

userCtr = 0
songCtr = 0
userDict = {}
songDict = {}
with open('train_triplets.txt') as f:
    for line in f:
        splitLine = line.split()
        # Handle user
        if splitLine[0] not in userDict:
            userCtr += 1
            userDict[splitLine[0]] = userCtr
        if splitLine[1] not in songDict:
            songCtr += 1
            songDict[splitLine[1]] = songCtr
        seq = (str(userDict[splitLine[0]]), str(songDict[splitLine[1]]), splitLine[2])
        modFile.write(','.join(seq)+"\n")


userFile = open('user.txt', 'w')
for key, value in userDict.items():
    seq = (str(key), str(value))
    userFile.write(' '.join(seq)+"\n")

songFile = open('song.txt', 'w')
for key, value in songDict.items():
    seq = (str(key), str(value))
    songFile.write(' '.join(seq)+"\n")


