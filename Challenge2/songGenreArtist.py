modfile = open('songGenreMatrix111.txt','w')

genreDict = {}
singleGenreDict = {}
artistDict = {}
a=[]
artistCount = 0
with open('modSongIDToGenre.txt') as modSongIDToGenre:
    for line in modSongIDToGenre:
        splitLine = line.split(',',1)
        g = splitLine[0]
        if g[-1] == '\n':
                g = g[:-1]
        g2 = splitLine[1]
        if g2[-1] == '\n':
                g2 = g2[:-1]
        genreDict[g] = g2
with open('singleGenreMapping.txt') as singleGenre:
    for line in singleGenre:
        splitLine = line.split(',',1)
        a.append(splitLine[0])

        g = splitLine[0]
        if g[-1] == '\n':
                g = g[:-1]
        g2 = splitLine[1]
        if g2[-1] == '\n':
                g2 = g2[:-1]
        singleGenreDict[g] = g2

with open('songToArtist.txt') as songToArtist:
    for line in songToArtist:
        splitLine = line.split(',',1)
        g = splitLine[0]
        if g[-1] == '\n':
                g = g[:-1]
        g2 = splitLine[1]
        if g2[-1] == '\n':
                g2 = g2[:-1]
        artistDict[g] = g2
        c = g2.split(',')[1]
        if int(c) > artistCount:
            artistCount = int(c)

print('Dictionary Built')

with open('song.txt') as f:
    for line in f:
        b = [0] * (len(a)+artistCount)
        splitLine = line.split()
        #artist
        if splitLine[1] in artistDict:
            artist = str(artistDict[splitLine[1]]).split(',')
            artistLocation = len(a)-1+int(artist[1])
            b[artistLocation] = 1

        #genre
        if splitLine[1] in genreDict:
           #print(genreDict[splitLine[1]])
            splitGenre = genreDict[splitLine[1]].split('	')
            for i in range(0, len(splitGenre)):
                for j in range(0,len(a)):
                    if splitGenre[i] == a[j]:
                        b[j] = 1

        seq = (splitLine[1], str(b)[:-1][1:].replace(' ',''))
        modfile.write(','.join(seq)+'\n')

