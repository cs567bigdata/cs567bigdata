import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname
from pyspark import SparkConf, SparkContext
import pandas as pd

trainFile = 'sp1Train.txt'
simFile = 'convSimilarityFile.txt'
outTrain = 'outTrainTrip1.txt'

# build track to song map
count=0
song2SimSong = {}
with open(simFile) as f:
    for line in f:
        line = line.split(',')
        line = [x.strip() for x in line]
        song2SimSong[line[0]]=line[1:]

cols = ['user','song','rating']
trainDf = pd.read_csv(trainFile,names=cols)
# 1368430
count = 0
users = pd.unique(trainDf.user.ravel())
for user in users:
    count+=1
    if count%10000==0:
       print('count = ' + str(count))
    usrDat = trainDf.loc[trainDf['user'] == user]
    usrSongList = usrDat['song'].tolist()
    usrRatingList = usrDat['rating'].tolist()
    for i in range(0,len(usrSongList)):
        try:
            simSongs = song2SimSong[str(usrSongList[i])]
        except:
            continue
        for simSong in simSongs:
           if simSong not in usrSongList:
               a = pd.DataFrame([[user,simSong,usrRatingList[i]]],columns=cols)
               trainDf = trainDf.append(a,ignore_index=True)

trainDf.to_csv(outTrain,index=False,header=False)

#            print(trainDf)
#with open(simFile) as f:
#    for line in f:
#        line = line.strip().split(',')
#        print(trainRdd.take(3))
#        print(trainRdd.map(lambda x: [x[1],x[0],x[2]]).groupByKey().collect())
#        break

