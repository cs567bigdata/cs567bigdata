import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname
import pandas as pd
import random as rd

trainFile = 'train_triplets-merge.txt'
simFile = 'convSimilarityFile.txt'
outTrain = 'outTrainTrip1Test-complete.txt'

# build track to song map
song2SimSong = {}
with open(simFile) as f:
    for line in f:
        line = line.split(',')
        line = [x.strip() for x in line]
        song2SimSong[line[0]]=line[1:]

cols = ['user','song','rating']
trainDf = pd.read_csv(trainFile,names=cols)
# 1368430
#print(trainDf)
count = 0
reductionPercent=1.0
users = pd.unique(trainDf.user.ravel())
randSamp = rd.sample(range(0,len(users)),int(len(users)*reductionPercent))
#print(len(randSamp))
users = [users[i] for i in randSamp]
#print(users[:10])
print('user len = ' + str(len(users)))

fidOut = open(outTrain,'w')
count=0
for userIdx in range(0,len(users)):
    count+=1
    if count%1000==0:
       print('count = ' + str(count))
    usrDat = trainDf.loc[trainDf['user'] == users[userIdx]]
    usrSongList = usrDat['song'].tolist()
    usrRatingList = usrDat['rating'].tolist()
    randSamp = rd.sample(range(0,len(usrSongList)),int(len(usrSongList)*reductionPercent))
    usrSongList = [usrSongList[i] for i in randSamp]
    for songIdx in range(0,len(usrSongList)):
        try:
            simSongs = song2SimSong[str(usrSongList[songIdx])]
        except:
            continue
        for simSong in simSongs:
           if simSong not in usrSongList:
               fidOut.write(str(users[userIdx]) +',' +str(simSong) +',' + str(usrRatingList[songIdx]) + "\n")
fidOut.close
