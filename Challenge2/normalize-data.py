import sys
import os.path

maxValueDict = {}
globalMax = 0

print('Finding local Max:')
for i in xrange(1,len(sys.argv)):
    print(sys.argv[i])
    with open(sys.argv[i]) as f:
        for line in f:
            splitLine = line.split(',')
            userId = splitLine[0]
            preference = float(splitLine[2].strip())
            if preference > globalMax:
                globalMax = preference
            if userId in maxValueDict:
                currentMax = maxValueDict[userId]
                if preference > currentMax:
                    maxValueDict[userId] = preference
            else:
                maxValueDict[userId] = preference
    f.close()

print('Making normalized copies of each file:')
for i in xrange(1,len(sys.argv)):
    fileName = os.path.splitext(sys.argv[i])[0]
    extension = os.path.splitext(sys.argv[i])[1]
    seq = (str(fileName),str("-normalized"),str(extension))
    normalizedFileName = str(fileName) + str("-normalized") + str(extension)
    modFile = open(normalizedFileName, 'w')
    print(normalizedFileName)
    with open(sys.argv[i]) as f:
        for line in f:
            splitLine = line.split(',')
            userId = splitLine[0]
            itemId = splitLine[1]
            max = maxValueDict[userId]
            preference = splitLine[2].strip()
            normalizedPreference = float(preference)/float(max)
            seq = (str(splitLine[0]), str(splitLine[1]), str(normalizedPreference))
            modFile.write(','.join(seq)+"\n")
    f.close()
    modFile.close()
