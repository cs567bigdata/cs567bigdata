import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname

from pyspark import SparkConf, SparkContext
from pyspark.mllib.recommendation import ALS,Rating

def parseRating(line):
    """
    Parses a rating record in MovieLens format userId::movieId::rating::timestamp .
    """
    fields = line.strip().split(',')
    return (int(fields[0]), int(fields[1]), float(fields[2]))

def parseMovie(line):
    """
    Parses a movie record in MovieLens format movieId::movieTitle .
    """
    fields = line.strip().split(',')
#    return fields[0], fields[1]
    return (int(fields[0]), int(fields[1]))#, float(fields[2]))

def loadTestData(ratingsFile):
    """
    Load ratings from file.
    """
    testData = sc.textFile(ratingsFile)
    return testData .map(lambda l: l.split(',')) \
        .map(lambda l: Rating(int(l[0]), int(l[1]), float(l[2])))

def computeRmse(model, data, n):
    """
    Compute RMSE (Root Mean Squared Error).
    """
    predictions = model.predictAll(data.map(lambda x: (x[0], x[1])))
    predictionsAndRatings = predictions.map(lambda x: ((x[0], x[1]), x[2])) \
        .join(data.map(lambda x: ((x[0], x[1]), x[2]))) \
        .values()
    return sqrt(predictionsAndRatings.map(lambda x: (x[0] - x[1]) ** 2).reduce(add) / float(n))
    # ...
def printRdd(x):
    print(x)

if __name__ == "__main__":
    if (len(sys.argv) != 3):
#        print "Usage: [usb root directory]/spark/bin/spark-submit --driver-memory 2g " + \
#          "MovieLensALS.py movieLensDataDir personalRatingsFile"
        print "Usage: spark-submit --driver-memory 4g " + \
          sys.argv[0] + " training_input testfile" 
        sys.exit(1)

    print("reading inputs")
    # set up environment
    conf = SparkConf() \
      .setAppName("MovieLensALS") \
      .set("spark.executor.memory", "4g")
    sc = SparkContext(conf=conf)

    # load personal ratings
    testDataRDD = loadTestData(sys.argv[2])

    # load ratings and movie titles

    userSongFilePath = sys.argv[1]

    # ratings is an RDD of (last digit of timestamp, (userId, movieId, rating))
    userSongRDD = sc.textFile(userSongFilePath).map(parseRating)

# DELETE ME:
#    userSongRDD = dict(sc.textFile(userSongFilePath).map(parseMovie).collect())
#    userSongRDD = dict(sc.textFile(userSongFilePath).map(parseMovie).collect())
#    print(len(dict(sc.textFile(userSongFilePath).map(parseMovie).collect())))

    # get counts of user song data
#    numRecords = userSongRDD.count()
#    print("num records = " + str(numRecords))
#    numUsers = userSongRDD.map(lambda r: r[0]).distinct().count()
#    numSongs = userSongRDD.map(lambda r: r[1]).distinct().count()
#    print "Got %d ratings from %d users on %d songs." % (numRecords, numUsers, numSongs)

    print("setting up trainer")
    numPartitions = 4
    training = userSongRDD.union(testDataRDD) \
      .repartition(numPartitions) #\
#      .cache()

    validation = userSongRDD \
      .repartition(numPartitions) #\
#      .cache()

    test = testDataRDD#.cache()

    numTraining = training.count()
    numValidation = validation.count()
    numTest = test.count()

#    print "Training: %d, validation: %d, test: %d" % (numTraining, numValidation, numTest) 
#    print training.first()
#    print validation.first()
#    print test.first()

    ranks = [8, 12]
    lambdas = [1.0, 10.0]
    numIters = [10, 20]
    bestModel = None
    bestValidationRmse = float("inf")
    bestRank = 0
    bestLambda = -1.0
    bestNumIter = -1

#    print("iterating to find best settings for trainer")
#    for rank, lmbda, numIter in itertools.product(ranks, lambdas, numIters):
#        print("training")
#        model = ALS.train(training, rank, numIter, lmbda)
#        print("computing RMSE")
#        validationRmse = computeRmse(model, validation, numValidation)
#        print "RMSE (validation) = %f for the model trained with " % validationRmse + \
#              "rank = %d, lambda = %.1f, and numIter = %d." % (rank, lmbda, numIter)
#        print("updating")
#        if (validationRmse < bestValidationRmse):
#            bestModel = model
#            bestValidationRmse = validationRmse
#            bestRank = rank
#            bestLambda = lmbda
#            bestNumIter = numIter

# Values found by performing iterative approch above
    bestRank = 12
    bestLambdas  = 1.0
    bestNumIter = 20
    print("train recommender")
    bestModel = ALS.train(training, bestRank, bestNumIter, bestLambdas)

    print("compute rmse")
    testRmse = computeRmse(bestModel, test, numTest)

    # evaluate the best model on the test set
    print "The best model was trained with rank = %d and lambda = %.1f, " % (bestRank, bestLambda) \
      + "and numIter = %d, and its RMSE on the test set is %f." % (bestNumIter, testRmse)
#The best model was trained with rank = 12 and lambda = -1.0, and numIter = 20, and its RMSE on the test set is 4.383649.

    # clean up

    sc.stop()
