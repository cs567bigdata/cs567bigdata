import hdf5_getters
h5 = hdf5_getters.open_h5_file_read('msd_summary_file.h5')
count = hdf5_getters.get_num_songs(h5)
f = open('song_metadata_song_id_title_track_id-complete.csv','w')
print("Starting...");
for i in range(0,count-1):
    if i==200000 or i==10000 or i==100000 or i==500000 or i==700000:
        print( hdf5_getters.get_year(h5,i) )
    f.write(hdf5_getters.get_song_id(h5,i))
    f.write(",")
    f.write(hdf5_getters.get_track_id(h5,i))
    f.write("\n")

print("Finished")
f.close()
h5.close()
