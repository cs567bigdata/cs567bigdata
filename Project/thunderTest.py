
#!/usr/bin/env python
import sys
import os.path as pth
from thunder import Colorize
from thunder import ThunderContext
from pyspark import SparkConf, SparkContext
from numpy import random
from scipy.ndimage.filters import gaussian_filter
import matplotlib.pyplot as plt
from scipy.ndimage import shift
from thunder import Registration
from thunder import Colorize


conf = SparkConf().setAppName('appName').setMaster('local').set("spark.driver.maxResultSize", "12g")
sc = SparkContext(conf=conf)
tsc = ThunderContext(sc)
#datapath = 'images/LC80340362015314LGN00/'
#datapath = 'images2/'
i = 1

while i < 500:

    #data = tsc.loadImages(datapath, inputFormat='png',startIdx= i-1 , stopIdx= i+1 )
    data = tsc.loadExample('mouse-images')
    Image = Colorize.image

    t = 500
    dx = gaussian_filter(random.randn(t), 50) * 25
    dy = gaussian_filter(random.randn(t), 50) * 25

#plt.plot(dx);
#plt.plot(dy);

    shifted = data.apply(lambda (k, v): (k, shift(v, (dx[k], dy[k]), mode='nearest', order=0)))

    reg = Registration('crosscorr')
    reg.prepare(shifted);


    ref = shifted.filterOnKeys(lambda k: k > 0 and k < 3).mean()
    reg.prepare(ref)
    model = reg.fit(shifted)

    corrected = model.transform(shifted)

    im1 = data[0]
    im2 = corrected[0]

    imgn = 'im'+str(i)+'.png'

    with open(imgn, 'wb') as f:
        f.write(im2)

    i = i+1

    print('image ' + str(i) + ' done')


##write to video

with open('im1.png', 'rb') as f:
    data = f.read()

height, width, layers = data.shape

video = cv2.VideoWriter('video.avi',-1,1,(width,height))
while i < 3:
    imgn = 'img'+str(i)+'.png'
    video.write(imgn)

cv2.destroyAllWindows()
video.release()

print('finished')

