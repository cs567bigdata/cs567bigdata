import cv2
import os
import numpy as np

img1 = cv2.imread('testr.jpg')
img2 = cv2.imread('testr2.jpg')
img3 = cv2.imread('testr3.jpg')
#cv2.imshow('image',img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()

img1gray = cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
img2gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
img3gray = cv2.cvtColor(img3,cv2.COLOR_BGR2GRAY)

size = img1.shape

warp_mode = cv2.MOTION_TRANSLATION

if warp_mode == cv2.MOTION_HOMOGRAPHY :
    warp_matrix = np.eye(3, 3, dtype=np.float32)
else:
    warp_matrix = np.eye(2, 3, dtype=np.float32)

number_of_iterations = 5000;

termination_eps = 1e-10;

criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations,  termination_eps)

(cc, warp_matrix) = cv2.findTransformECC (img1gray,img2gray,warp_matrix, warp_mode, criteria)
(cc2, warp_matrix2) = cv2.findTransformECC (img1gray,img3gray,warp_matrix, warp_mode, criteria)

if warp_mode == cv2.MOTION_HOMOGRAPHY :
    # Use warpPerspective for Homography
    im2_aligned = cv2.warpPerspective (img2, warp_matrix, (size[1],size[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    im3_aligned = cv2.warpPerspective (img3, warp_matrix2, (size[1],size[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
else:
    # Use warpAffine for Translation, Euclidean and Affine
    im2_aligned = cv2.warpAffine(img2, warp_matrix, (size[1],size[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP);
    im3_aligned = cv2.warpAffine(img3, warp_matrix2, (size[1],size[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP);

# Show final results
#cv2.namedWindow('Image 1', cv2.WINDOW_NORMAL)
#cv2.imshow("Image 1", img1)
#cv2.namedWindow('Image 2', cv2.WINDOW_NORMAL)
#cv2.imshow("Image 2", img2)
#cv2.namedWindow('Aligned Image 2', cv2.WINDOW_NORMAL)
#cv2.imshow("Aligned Image 2", im2_aligned)
#cv2.waitKey(0)

height, width, layers = img1.shape

video = cv2.VideoWriter('video.avi',-1,1,(width,height))

video.write(img1)
video.write(im2_aligned)
video.write(im3_aligned)

cv2.destroyAllWindows()
video.release()

print('finished')
