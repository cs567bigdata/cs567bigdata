import sys
import os
from threading import Thread

from LandsatExtractor import LandsatExtractor

def extract(name,zipFile,bands, path=''):
    print name + ' is working ' + zipFile
    le = LandsatExtractor(zipFile)
    le.extractBands(bands)

if __name__=='__main__':
#   crude parse ags
    if len(sys.argv) != 3:
        print('Usage: ' + sys.argv[0] + ' scenelist dir/with/scenes')
        sys.exit(1)
    sceneList = sys.argv[1]
    workingDir = sys.argv[2]

# RGB = bands 4 3 2 respectively, extract them from all data sets
    with open(sceneList) as f:
        scenes = f.readlines()

    bands = [2, 3, 4]
    count = 0
    path = workingDir
    threadList = []
    for scene in scenes:
        # get zip
        name = 'thread-'+str(count)
        print('starting ' +name)
        zipFile = os.path.join(workingDir, scene.strip() + '.tar.bz')
        threadList.append(Thread(extract,(name,zipFile,bands,path)))
        count+=1

    for indThread in threadList:
        indThread.start()
    for indThread in threadList:
        indThread.join()
    print('done =======================')

