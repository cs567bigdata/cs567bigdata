import os.path
import tarfile

#['LC80330362015243LGN00_B1.TIF', 'LC80330362015243LGN00_B2.TIF', 'LC80330362015243LGN00_B3.TIF', 'LC80330362015243LGN00_B4.TIF', 'LC80330362015243LGN00_B5.TIF', 'LC80330362015243LGN00_B6.TIF', 'LC80330362015243LGN00_B7.TIF', 'LC80330362015243LGN00_B8.TIF', 'LC80330362015243LGN00_B9.TIF', 'LC80330362015243LGN00_B10.TIF', 'LC80330362015243LGN00_B11.TIF', 'LC80330362015243LGN00_BQA.TIF', 'LC80330362015243LGN00_MTL.txt']


class LandsatExtractor():
    _txtFileExt = '_MTL.txt'
    _Band = '_MTL.txt'
    def __init__(self, zipFile):
        self._path = zipFile
        tmp = os.path.basename(zipFile) 
        self._filename = tmp[:tmp.find('.')]
        self._ext = tmp[tmp.find('.'):]

    def _band(self, num):
        if not (num == 'QA' or (num >0 and num <=11)):
            raise Exception('bandnum out of range', 'num = ' + str(num) +
                ' must be between 1 and 11 or QA')
        return self._filename + '_B' + str(num) + '.TIF'

    def extractBand(self, bandNum, path=''):
        tf = tarfile.open(self._path)
        tf.extract(self._band(bandNum),path)
        tf.close()

    def extractBands(self, bandNums, path=''):
#        tf = tarfile.open(self._path)
        with tarfile.open(self._path) as tf:
            for band in bandNums:
                print('extracting ' + self._band(band))
                tf.extract(self._band(band),path)
#        tf.close()

#    def getInfo(self):
#        tf = tarfile.open(self._path)
#        print(tf.getmember(self._filename+self._txtFileExt))


if __name__ == '__main__':
    zipFile = '/users/telabru/data/LC80330362015179LGN00.tar.bz'
    ex = LandsatExtractor(zipFile)
    print('extracting band 2')
    ex.extractBand(2)
    
#    zipFile = '/users/telabru/data/LC80330362015243LGN00.tar.bz'
#    LandsatExtractor(zipFile)

